#!/usr/bin/python3

#install (some of these, debug it)
# sudo apt-get install git curl libsdl2-mixer-2.0-0 libsdl2-image-2.0-0 libsdl2-2.0-0
# python -m pip install pygame

# fixing alsa error:
# sudo nano /etc/pulse/daemon.conf
# default-fragments = 5
# default-fragment-size-msec = 25

#!/usr/bin/python3

import os, time, redis, json
os.environ['SDL_AUDIODRIVER'] = 'alsa'
import pygame
pygame.mixer.init()

# Connect to the redis queue
r = redis.from_url(os.getenv("REDIS_URL", "redis://localhost:6379"))

# Ignore meta data
p = r.pubsub(ignore_subscribe_messages=True)

# Get all channels from script arguments
channel = "toPi"

# Subscribe to the queue
p.psubscribe(channel)

# Loop over the queue
while True:
    message = p.get_message()

    # Process message if it actually contains data
    if message and type(message['data']) == bytes:

        # Get the data
        try:
            data = json.loads(message['data'])
        except ValueError as e:
            data = message['data'].decode('ascii')
        channel = message['channel'].decode('ascii')
        print(f"[{channel}] --> {data}")
        pygame.mixer.music.load("./mp3/ufo.mp3")
        pygame.mixer.music.play()

    time.sleep(0.001)  # be nice to the system :)



#while pygame.mixer.music.get_busy() == True:
#    continue
