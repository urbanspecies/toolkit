#!/usr/bin/python3
import os, sys ,time, redis, json

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print(f'To use, add parameters to this script!\n'
              f'Some examples:\n\n'
              f'  {sys.argv[0]} channel_name\n\n'
              f'  python3 {sys.argv[0]} channel_name other_channel')
    else:
        # Connect to the redis queue
        r = redis.from_url(os.getenv("REDIS_URL", "redis://localhost:6379"))
        
        # Ignore meta data
        p = r.pubsub(ignore_subscribe_messages=True)
        
        # Get all channels from script arguments
        channels = sys.argv[1:]
        
        # Subscribe to the queue
        p.psubscribe(*channels)
    
        # Loop over the queue
        while True:
            message = p.get_message()
    
            # Process message if it actually contains data
            if message and type(message['data']) == bytes:
                
                # Get the data
                try:
                    data = json.loads(message['data'])
                except ValueError as e:
                    data = message['data'].decode('ascii')
                channel = message['channel'].decode('ascii')
                print(f"[{channel}] --> {data}")
    
            time.sleep(0.001)  # be nice to the system :)