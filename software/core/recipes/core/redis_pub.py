#!/usr/bin/python3
import os, sys ,time, redis, json

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print(f'To use, add parameters to this script!\n'
              f'Don\'t forget to add single quotes when using json!\n'
              f'Some examples:\n\n'
              f'  {sys.argv[0]} channel_name \'{{"data":"hi"}}\' here\n\n'
              f'  python3 {sys.argv[0]} channel_name,other_channel your text here')
    else:
        # Connect to the redis queue
        r = redis.from_url(os.getenv("REDIS_URL", "redis://localhost:6379"))
        
        # Get the channel list from script arguments and prepare data
        channels = sys.argv[1].split(',')
        data = ' '.join(sys.argv[2:])
        try:
            json.loads(data)
        except ValueError as e:
            _data = {}
            _data["debug"] = data
            data = json.dumps(_data)
                  
        # Publish on each channel in the list
        for channel in channels:
            r.publish(channel, data)
            print(f'[{channel}] <-- {data}')
