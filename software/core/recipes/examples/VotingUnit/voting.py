#!/usr/bin/python3

# arduino pins to button text mapping
pins_to_options = {
    6  : "Art et Culture", #geel
    9  : "Embellissement", #oranje
    14 : "Mobilité", #blauw
    15 : "Défi Climat", #groen
    18 : "Jeunesse", #roze
}

pins_to_sounds = {
    6  : "clap", #geel
    9  : "click", #oranje
    14 : "drum", #blauw
    15 : "pan", #groen
    18 : "perc", #roze
}

pins_to_animations = {
    6  : 3, #geel
    9  : 4, #oranje
    14 : 2, #blauw
    15 : 1, #groen
    18 : 5, #roze
}

# Default count, overwritten by json file
vote_count = {
    6  : 0,
    9  : 0,
    14 : 0,
    15 : 0,
    18 : 0,
}

# imports and external libraries
import os, sys ,time, redis, json

fontdir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'font')
libdir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'lib')

if os.path.exists(libdir):
    sys.path.append(libdir)

import logging
from waveshare_epd import epd7in5_V2
import time
from PIL import Image,ImageDraw,ImageFont
import traceback

# logging, please change for PROD
#logging.basicConfig(level=logging.DEBUG)

# settings
FONT_NAME = 'SpaceGrotesk-Regular' #'OpenSans-Bold'
VOTE_FONT_SIZE = 40
TOTAL_VOTE_SIZE = 80
TOP_CLEARANCE = 200
TOP_MARGIN = 20
VOTE_SPACE = 50
LEFT_MARGIN = 20
VOTE_COUNT_POSITION = 375


epd = epd7in5_V2.EPD()
epd.init()

def update_e_ink(votes):

    #epd.Clear()
    fontTotal = ImageFont.truetype(os.path.join(fontdir, "%s.ttf" % FONT_NAME), TOTAL_VOTE_SIZE)
    fontOption = ImageFont.truetype(os.path.join(fontdir, "%s.ttf" % FONT_NAME), VOTE_FONT_SIZE)

    # build new image
    newImage = Image.new('1', (epd.height, epd.width), 255)
    draw = ImageDraw.Draw(newImage)

    current_position = TOP_CLEARANCE
    total_votes = 0
    for option in votes.keys():
        draw.text((LEFT_MARGIN, current_position), "%s" % option, font=fontOption, fill=0)
        draw.text((VOTE_COUNT_POSITION, current_position), "%d" % votes[option], font=fontOption, fill=0)
        total_votes += votes[option]
        current_position += VOTE_FONT_SIZE + VOTE_SPACE

    draw.text((LEFT_MARGIN, TOP_MARGIN), "%d votes" % total_votes, font=fontTotal, fill=0)

    # rotate image for display
    finalImage = newImage.transpose(Image.ROTATE_90)

    # draw onto display
    epd.display(epd.getbuffer(finalImage))
    #epd.sleep()
    #epd.Dev_exit()




try:
    with open('data.json') as json_file:
        # object_hook -> Because we want numeric keys! (that doesn't exist in json techincally)
        vote_count = json.load(json_file, object_hook=lambda x: {int(k):v for k,v in x.items()} if isinstance(x, dict) else x)
except IOError:
    print("File doesn't exist")

# Connect to the redis queue
r = redis.from_url(os.getenv("REDIS_URL", "redis://localhost:6379"))

# Ignore meta data
p = r.pubsub(ignore_subscribe_messages=True)
# Subscribe to the queue
p.psubscribe("fromMesh")

# Loop over the queue
while True:
    message = p.get_message()

    # Process message if it actually contains data
    if message and type(message['data']) == bytes:

        # Get the data
        try:
            data = json.loads(message['data'])
            _type = data.get('type', "Unknown")
            pin  = data.get('pin', -1)
            if _type == 'button' and pin in pins_to_options:
                vote_count[pin] = vote_count[pin] + 1

                # Write all votes away in data file
                with open('data.json', 'w') as outfile:
                    json.dump(vote_count, outfile)

                _data = {"type":"PlayAnimation", "id": 40, "memorybank":pins_to_animations[pin], "repeat":1, "speed":45}
                data = json.dumps(_data)
                r.publish("toMesh", data)

                _data = {"play":pins_to_sounds[pin]}
                data = json.dumps(_data)
                r.publish("toPi", data)

                overview_votes = dict(zip(pins_to_options.values(), vote_count.values()))
                update_e_ink(overview_votes)

                #print(f"{pins_to_options[pin]}")
        except ValueError as e:
            pass
    time.sleep(0.001)  # be nice to the system :)
