#!/usr/bin/python3
import os, sys ,time, redis, json

# We import all the mqtt stuff as_mqtt so we can use mqtt as our object to publish from
import paho.mqtt.client as _mqtt
from paho.mqtt.client import ssl

MQTT_HOST = 'mqtt.urbanspecies.org'
MQTT_PORT = 8883
MQTT_USER = 'urbanspecies'
MQTT_PASS = ''
MQTT_CLIENT_ID = 'python_pms5003'


# We define a disconnect handler and kill the script: systemd can restart it
def on_disconnect(client, userdata, rc):
    if rc != 0:
        exit("MQTT had an unexpected disconnect.")

mqtt = _mqtt.Client(client_id=MQTT_CLIENT_ID)
mqtt.username_pw_set(username=MQTT_USER,	password=MQTT_PASS)
mqtt.tls_set_context(ssl.create_default_context())
mqtt.connect(host=MQTT_HOST,	port=MQTT_PORT)


# return the "urban species air quality index" (less advanced as the official one)
def calculateUSAQI(PM10, PM25, PM100):
    PM100mean = [0,8,16,24,40,56,63,79,95,111]
    PM25mean = [0,5,10,15,25,35,40,50,60,70]
    if (PM25 > PM25mean[9]) or (PM100 > PM100mean[9]): #if we have very high values return 9
        return 9
    for i in range(0,8): #although there are 10 values, we only count to
        if (PM100mean[i+1] > PM100) and (PM25mean[i+1] > PM25):
            return i
        if i == 8:
            return 9

# Connect to the redis queue
r = redis.from_url(os.getenv("REDIS_URL", "redis://localhost:6379"))

# Ignore meta data
p = r.pubsub(ignore_subscribe_messages=True)

# Subscribe to the queue
channel = "fromMesh"
p.psubscribe(channel)

# Loop over the queue
while True:
    message = p.get_message()

    # Process message if it actually contains data
    if message and type(message['data']) == bytes:

        # Get the data
        try:
            data = json.loads(message['data'].rstrip())
            _type = data.get('type', "undefined")
            if _type == "pms5003":
                del data["id"] #we don't need the nodeID
                data["device_id"]        = "renovas_stephenson"
                data["measurement"]      = "airquality"
                data["target"]           = "renovas_stephenson"
                data["sensor_position"]  = "1th floor"
                data["sensor_algorithm"] = "standard"
                data["USAQI"]            = calculateUSAQI(data["PM10"], data["PM25"], data["PM100"])
                json_data = json.dumps(data)
                mqtt.publish('pms5003', json_data)
                print(f"{data}")
        except ValueError as e:
            data = message['data'].decode('ascii')
            print(f"[{channel}] invalid data --> {data}")

    time.sleep(0.001)  # be nice to the system :)
